# CHANGELOG

### 2016-12-17
* add ProfilerPanel

### 2016-12-15
* add ConsolePanel
* change config format

### 2016-12-10
* add PanelSelector
* fix vertical scroll
* move show Slim Container to panel
* delete all barDump 

### 2016-12-09
* add Included Files Panel

### 2016-11-29
* removed from dependencies: 
  - slim/twig-view
  - illuminate/database
  - version
* add Screen Shots
